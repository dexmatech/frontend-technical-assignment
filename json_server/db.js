const companies = require('./resources/companies.json');

module.exports = () => ({
    companies
});
