import GetCompaniesClient, { CompaniesResult } from '../domain/client/getCompaniesClient';
import { Result } from '../domain/communication';
import Company from '../domain/entity/company';

export type CountriesResult = Promise<Result<string[]>>;

export type GetAllCountriesSortedUsecase = () => CountriesResult;

const allCountriesSortedUsecase =
    (getCompaniesClient: GetCompaniesClient): GetAllCountriesSortedUsecase =>
    (): CountriesResult =>
        getCompaniesClient().then((companiesResult: CompaniesResult) => {
            if (companiesResult.isFailure()) {
                return Result.failure<string[]>(companiesResult.getError()!);
            }

            return Result.success<string[]>(
                companiesResult
                    .getData()!
                    .map((company: Company) => company.countries)
                    .reduce((a: string[], b: string[]) => Array.from(new Set([...a, ...b])))
                    .sort((a: string, b: string) => (a > b && 1) || -1)
            );
        });

export default allCountriesSortedUsecase;
