import { Result } from '../domain/communication';
import Company from '../domain/entity/company';
import allCountriesSortedUsecase from './getAllCountriesSortedUsecase';

describe('Get All Countries Sorted Usecase', () => {
    const mockedGetCompaniesClient = jest.fn();
    test('should get all countries successfully', async () => {
        mockedGetCompaniesClient.mockImplementation(() =>
            Promise.resolve(
                Result.success([
                    new Company('bb44036a-a5f2-41c6-99f7-d658ec03d271', 'Covance', 3.9, ['Iran', 'Cambodia'], ['black']),
                    new Company(
                        '646fadb5-7a1a-4827-b240-859751eba2c0',
                        'Under Armour',
                        3,
                        ['Cambodia', 'Iraq', 'Peru', 'Bouvet Island (Bouvetoya)', 'Antigua and Barbuda'],
                        ['green']
                    ),
                ])
            )
        );

        const result = await allCountriesSortedUsecase(mockedGetCompaniesClient)();

        expect(mockedGetCompaniesClient).toHaveBeenNthCalledWith(1);
        expect(result).toEqual(Result.success(['Antigua and Barbuda', 'Bouvet Island (Bouvetoya)', 'Cambodia', 'Iran', 'Iraq', 'Peru']));
    });

    test('should forward error when recieved error from the client', async () => {
        mockedGetCompaniesClient.mockImplementation(() =>
            Promise.resolve(Result.failure({ error: new Error('Error form the client'), status: 503 }))
        );

        const result = await allCountriesSortedUsecase(mockedGetCompaniesClient)();

        expect(mockedGetCompaniesClient).toHaveBeenNthCalledWith(1);
        expect(result).toEqual(Result.failure({ error: new Error('Error form the client'), status: 503 }));
    });
});
