import { when, WhenMock } from 'jest-when';

class MockFunctions {
    static mockFetchCall =
        (fetch: jest.MockInstance<unknown, unknown[]>) =>
        (extendedUrl: string, params: unknown, status: number, body: unknown): WhenMock =>
            when(fetch)
                .calledWith(extendedUrl, params)
                .mockReturnValue(
                    new Promise((resolveCall) =>
                        resolveCall({
                            ok: status < 300,
                            json: () => new Promise((resolvePayload) => resolvePayload(body)),
                            status,
                        })
                    )
                );
}

export default MockFunctions;
