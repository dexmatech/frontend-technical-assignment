import Company from '../../../../domain/entity/company';
import MockFunctions from '../../../../resources/mockFunctions';
import getCompaniesApiClient from './getCompaniesClient';

describe('Get Companies Client', () => {
    const mockFetch = jest.fn();
    const baseUrl = 'http://test';
    const path = 'companies';
    const url = `${baseUrl}/${path}`;

    test('should get companies', async () => {
        MockFunctions.mockFetchCall(mockFetch)(url, {}, 200, [
            {
                id: 'bb44036a-a5f2-41c6-99f7-d658ec03d271',
                name: 'Covance',
                rating: 3.9,
                countries: ['Iran'],
                sectors: ['black'],
            },
            {
                id: '646fadb5-7a1a-4827-b240-859751eba2c0',
                name: 'Under Armour',
                rating: 3,
                countries: ['Cambodia', 'Iraq', 'Peru', 'Bouvet Island (Bouvetoya)', 'Antigua and Barbuda'],
                sectors: ['green'],
            },
        ]);

        const result = await getCompaniesApiClient(mockFetch, baseUrl)();

        expect(result.isSuccess()).toBeTruthy();
        expect(result.getData()).toEqual([
            new Company('bb44036a-a5f2-41c6-99f7-d658ec03d271', 'Covance', 3.9, ['Iran'], ['black']),
            new Company(
                '646fadb5-7a1a-4827-b240-859751eba2c0',
                'Under Armour',
                3,
                ['Cambodia', 'Iraq', 'Peru', 'Bouvet Island (Bouvetoya)', 'Antigua and Barbuda'],
                ['green']
            ),
        ]);
    });

    test('should return error when receiving 503 form HttpClient', async () => {
        MockFunctions.mockFetchCall(mockFetch)(url, {}, 503, {});

        const result = await getCompaniesApiClient(mockFetch, baseUrl)();

        expect(result.isFailure()).toBeTruthy();
        expect(result.getError()).toEqual({ error: new Error(''), status: 503 });
    });
});
