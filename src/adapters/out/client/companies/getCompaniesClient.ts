import { HttpClient, Result } from '../../../../domain/communication';
import Company from '../../../../domain/entity/company';
import GetCompaniesClient from '../../../../domain/client/getCompaniesClient';

const getCompaniesApiClient =
    (httpClient: HttpClient, baseUrl: string): GetCompaniesClient =>
    () =>
        httpClient(`${baseUrl}/companies`, {})
            .then(async (response) =>
                response.ok
                    ? response
                          .json()
                          .then(asCompanies)
                          .then((result: Company[]) => Result.success<Company[]>(result))
                          .catch(() => Result.failure<Company[]>({ error: new Error(), status: 500 }))
                    : Result.failure<Company[]>({ error: new Error(await response.text()), status: response.status })
            )
            .catch(() => Result.failure<Company[]>({ error: new Error(), status: 503 }));

const asCompanies = (companiesJson: CompanyJson[]) =>
    companiesJson.map(
        (companyJson: CompanyJson) =>
            new Company(companyJson.id, companyJson.name, companyJson.rating, companyJson.countries, companyJson.sectors)
    );

type CompanyJson = {
    id: string;
    name: string;
    rating: number;
    countries: string[];
    sectors: string[];
};

export default getCompaniesApiClient;
