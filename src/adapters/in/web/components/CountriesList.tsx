import { ReactElement, useCallback, useEffect, useState } from 'react';
import { Result as ResultComponent, Loading, Grid, Row, Cell, Button, ResultVariants } from '@dexma/ui-components';

import { useDependencies } from '../App';

type CountryState = {
    loading: boolean;
    countries: string[];
    error: boolean;
};

const CountriesList = (): ReactElement => {
    const { getCountriesUsecase } = useDependencies();
    const [countryState, setCountryState] = useState<CountryState>({ loading: true, countries: [], error: false });

    const getAllCountriesSorted = useCallback(
        () =>
            getCountriesUsecase().then((result) => {
                if (result.isSuccess()) {
                    setCountryState({ loading: false, countries: result.getData()!, error: false });
                } else if (result.isFailure()) {
                    setCountryState({ loading: false, countries: [], error: true });
                }
            }),
        [getCountriesUsecase]
    );

    useEffect(() => {
        getAllCountriesSorted();
    }, [getAllCountriesSorted]);

    if (countryState.loading) {
        return <Loading isLoading size={50} />;
    }

    if (countryState.error) {
        return <ResultComponent variant={ResultVariants.ERROR} />;
    }

    return (
        <Grid>
            {countryState.countries.map((country: string, index: number) => (
                <Row key={index}>
                    <Cell>
                        <Button text={country} variant='link' onClick={() => alert(country)} />
                    </Cell>
                </Row>
            ))}
        </Grid>
    );
};

export default CountriesList;
