import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import App from './App';

import companies from '../../../resources/fixtures/companies.json';

type MockedFetch = jest.Mock<Promise<Response>, [input: RequestInfo, init?: RequestInit]>;

const port = 4001;
const baseUrl = `http://test:${port}`;

describe('Integration', () => {
    let mockedFetch: MockedFetch;

    const structure = (mockedFetch: MockedFetch) => <App httpClient={mockedFetch} baseUrl={baseUrl} />;

    jest.spyOn(window, 'alert').mockImplementation(() => {});

    beforeEach(() => {
        mockedFetch = jest.fn((input: RequestInfo) => {
            switch (input) {
                case 'http://test:4001/companies':
                default:
                    return Promise.resolve(new Response(JSON.stringify(companies)));
            }
        });
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    test('renders, loads countries and click one of them', async () => {
        render(structure(mockedFetch));

        expect(screen.getByText('Energy efficiency rating')).toBeInTheDocument();

        expect(screen.getByText('Countries')).toBeInTheDocument();

        expect(screen.getByText('Top 5 companies')).toBeInTheDocument();

        expect(screen.getByTestId('spinner')).toBeInTheDocument();

        expect(mockedFetch).toHaveBeenNthCalledWith(1, `${baseUrl}/companies`, {});

        await waitFor(() => {
            expect(screen.getByText('Afghanistan')).toBeInTheDocument();
        });
        const japan = screen.getByText('Japan');
        expect(japan).toBeInTheDocument();
        expect(screen.getByText('Zimbabwe')).toBeInTheDocument();

        fireEvent.click(japan);
        expect(window.alert).toHaveBeenNthCalledWith(1, 'Japan');
    });
});
