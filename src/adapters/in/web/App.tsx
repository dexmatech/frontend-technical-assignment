import React from 'react';
import { Theme, GlobalStyle, Grid, Row, Cell, Heading } from '@dexma/ui-components';

import createDependencyContext, { getUseDependencies } from './context/dependenciesContext';
import { HttpClient } from '../../../domain/communication';
import allCountriesSortedUsecase, { GetAllCountriesSortedUsecase } from '../../../use-case/getAllCountriesSortedUsecase';
import getCompaniesApiClient from '../../out/client/companies/getCompaniesClient';
import CountriesList from './components/CountriesList';

const DEPENDENCIES = 'DEPENDENCIES';
export interface IDependencies {
    getCountriesUsecase: GetAllCountriesSortedUsecase;
}

export const useDependencies = () => getUseDependencies<IDependencies>(DEPENDENCIES)();

type AppProps = {
    httpClient: HttpClient;
    baseUrl: string;
};

const App = (props: AppProps) => {
    const { httpClient, baseUrl } = props;

    const getCompaniesClient = getCompaniesApiClient(httpClient, baseUrl);
    const { Provider: DependencyProvider } = createDependencyContext<IDependencies>(DEPENDENCIES, {
        getCountriesUsecase: allCountriesSortedUsecase(getCompaniesClient),
    });

    return (
        <Theme options={{ primary: '#555555' }}>
            <GlobalStyle />
            <Grid fluid>
                <Row>
                    <Cell xs={12}>
                        <Heading type='h1' text='Energy efficiency rating' />
                    </Cell>
                </Row>

                <Row>
                    <Cell xs={6}>
                        <Heading type='h1' text='Countries' />
                    </Cell>
                    <Cell xs={6}>
                        <Heading type='h1' text='Top 5 companies' />
                    </Cell>
                </Row>

                <Row>
                    <Cell xs={6}>
                        <DependencyProvider>
                            <CountriesList />
                        </DependencyProvider>
                    </Cell>

                    <Cell xs={6} style={{ alignSelf: 'flex-start' }}>
                        TODO
                    </Cell>
                </Row>
            </Grid>
        </Theme>
    );
};

export default App;
