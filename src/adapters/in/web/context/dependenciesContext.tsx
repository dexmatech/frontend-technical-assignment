import React, { createContext, useContext } from 'react';

type DependencyContextProps<T> = {
    dependencies?: T;
    children: React.ReactNode;
};

interface DependencyContextType<T> {
    Provider: React.FC<DependencyContextProps<T>>;
    useDependencies: () => T;
}

const createDependencyContext = <T,>(name: string, defaultDependencies: T): DependencyContextType<T> => {
    const DependencyContext = createContext<T | undefined>(undefined);

    const DependencyProvider: React.FC<DependencyContextProps<T>> = ({ dependencies, children }) => (
        <DependencyContext.Provider value={dependencies}>{children}</DependencyContext.Provider>
    );

    const useDependencies = () => {
        const context = useContext(DependencyContext);
        if (!context) {
            return defaultDependencies;
        }
        return context;
    };

    useDependenciesMap.set(name, useDependencies);

    return { Provider: DependencyProvider, useDependencies };
};

const useDependenciesMap: Map<string, unknown> = new Map();

export const getUseDependencies = <T,>(name: string): (() => T) => {
    if (!useDependenciesMap.has(name)) throw Error('Missing dependency injector');
    return useDependenciesMap.get(name) as () => T;
};

export default createDependencyContext;
