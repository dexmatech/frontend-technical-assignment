class Company {
    constructor(
        readonly id: string,
        readonly name: string,
        readonly rating: number,
        readonly countries: string[],
        readonly sectors: string[]
    ) {}
}

export default Company;
