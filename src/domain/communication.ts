export class Result<T> {
    private data: T | undefined;
    private error: ErrorData | undefined;

    constructor(data?: T, error?: ErrorData) {
        this.data = data;
        this.error = error;
    }

    public static success<T>(data: T) {
        return new Result<T>(data);
    }

    public static failure<T>(error: ErrorData) {
        return new Result<T>(undefined, error);
    }

    public getData(): T | undefined {
        return this.data;
    }

    public getError(): ErrorData | undefined {
        return this.error;
    }

    public isSuccess(): boolean {
        return !!this.data;
    }

    public isFailure(): boolean {
        return !!this.error;
    }
}

export type ErrorData = { error: Error; status: number };
export type HttpClient = (input: RequestInfo, init?: RequestInit) => Promise<Response>;
