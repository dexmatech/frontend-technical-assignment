import { Result } from '../communication';
import Company from '../entity/company';

export type CompaniesResult = Result<Company[]>;
type GetCompaniesClient = () => Promise<CompaniesResult>;
export default GetCompaniesClient;
