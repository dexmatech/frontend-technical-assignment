import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './adapters/in/web/App';

const container = document.getElementById('root') as HTMLElement;
const root = createRoot(container);

root.render(
    <React.StrictMode>
        <App httpClient={fetch} baseUrl='http://localhost:4000' />
    </React.StrictMode>
);
